# Mi Primer Dockerfile 

presentacion [link](https://docs.google.com/presentation/d/17Xpk3lviU3tv_ecHKrxjkYY76PQ2ijdswcZd7dJh07s/edit?usp=sharing)

conjuntos de ejemplos de como empezar a usar docker.

comandos utiles

build 
```
docker build -t "namecontainer" .
```
revisar contenedores corriendo
```
docker ps 
```
revisar todos los contenedores aun los que estan detenidos
```
docker ps -a
```
listar volumenes
```
docker volume ls 
```

arrancar o correr
```
docker run
```
detener
```
docker stop "contenedornombre/ o ID"
```


## ejemplo1

ejemplo de web server Apache

construir primer contenedor
```
docker build -t ejemplo1 .
```
levantar contenedor 
```
docker run  -p 8080:80 ejemplo1
```
montar carpeta customizada
```
docker run -p 8080:80 -v "rutalocal:rutacontenedor" ejemplo1
```
```
docker run -p 8080:80 -v /user/document/html:/var/www/html/ ejemplo1
```


## ejemplo2

node js skeleton app

build
```
docker build -t ejemplo2 .
```

run 
```
docker run -p 8080:8080 ejemplo2
```

## ejemplo3
construir contenedor a partir de repo github

construir
```
docker build -t ejemplo3 .
```

run 
```
docker run -p 8080:8080 ejemplo2
```

##### ejemplo3.1

construir un docker compose file a partir de los 3 ultimos ejemplos

levantar contenedores
```
dockercompose up
```


## ejemplo4

levantar stack local

```
docker-compose up
```
correr en background
```
docker-compose up -D
```

## ejemplo5

ejemplo de docker swarm

inicializar docker swarm
```
docker swarm init
```

arrancar stack
```
docker stack deploy -c demostack.yml App1
```


chequear stack
```
docker stack services App1
```
revisar que este corriendo o ver via navegador
```
curl localhost
```


#### escalar 

escalar por medio de modificacion de yml
```
docker stack deploy -c demostackreplicas.yml App1
```

escalar por medio de cli
```
docker service scale "servicio"=3
```





# Referencias 

Instalar Docker

```
https://docs.docker.com/install/
```
